import { Injectable } from '@angular/core';

@Injectable({ providedIn: 'root' })
export class VideoDbServiceService {

  private videos: string[]

  constructor() {
    this.videos = ["Peter", "Ilse", "Hammer"]
  }

  getVideos() {
    return this.videos
  }

}
