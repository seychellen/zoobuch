import { TestBed } from '@angular/core/testing';

import { VideoDbServiceService } from './video-db-service.service';

describe('VideoDbServiceService', () => {
  let service: VideoDbServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(VideoDbServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
