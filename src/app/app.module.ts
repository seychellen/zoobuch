import { VideoDbServiceService } from './shared/video-db-service.service';
import { NgModule, Component } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { VideoComponent } from './video/video.component';
import { WebsiteComponent } from './website/website.component';

const meineRouten: Routes = [
  { path: 'video/:id', component: VideoComponent },
  { path: 'website', component: WebsiteComponent },
  { path: '**', redirectTo: '/video/42' }
];

@NgModule({
  declarations: [
    AppComponent,
    VideoComponent,
    WebsiteComponent
  ],
  imports: [
    RouterModule.forRoot(meineRouten),
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [VideoDbServiceService, { provide: 'VideoComponentConfig', useValue: 'meineKonfig' }],
  bootstrap: [AppComponent]
})
export class AppModule { }
