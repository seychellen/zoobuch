import { Component } from '@angular/core';
import { Animal } from './animal';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'zoobuch';
  animals : Animal[] = [ 
    {
      name: "Jörg",
      type: "Maulwurf"
    },
    {
      name: "Peter",
      type: "Regenwurm"
    },
  ]

  newAnimal : Animal = this.initNewAnimal()

  initNewAnimal() {
    return <Animal>{}
  }

  deleteAnimal(animal: Animal): void {
    this.animals = this.animals.filter(a => a != animal)
  }

  addAnimal() {
    this.animals.push(this.newAnimal)
    this.newAnimal = this.initNewAnimal()
  }

  meinEmitterHandler() {
    window.alert('Das ist was gekommen!')
  }

}
