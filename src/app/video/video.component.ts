import { VideoDbServiceService } from './../shared/video-db-service.service';
import { Component, OnInit, Output, EventEmitter, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-video',
  templateUrl: './video.component.html',
  styleUrls: ['./video.component.css']
})
export class VideoComponent implements OnInit {

  url = 'http://www.google.de'
  videos : string[]
  @Output() meinEmitter = new EventEmitter()

  constructor(private route: ActivatedRoute, videoDB: VideoDbServiceService, @Inject('VideoComponentConfig') config: string) {
    this.videos = videoDB.getVideos()
    console.log("Config: " + config)
  }

  ngOnInit(): void {
    console.log(this.route.snapshot.params['id'])
  }

  myClickHandler(e: Event) {
    console.log(e)
    this.meinEmitter.emit()
  }

}
