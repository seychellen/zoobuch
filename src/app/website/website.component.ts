import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { retry } from 'rxjs/operators';

@Component({
  selector: 'app-website',
  templateUrl: './website.component.html',
  styleUrls: ['./website.component.css']
})
export class WebsiteComponent implements OnInit {
  myVideoID = 42
  priceInEur: number = 0;

  constructor(private client: HttpClient) { }

  ngOnInit(): void {
    this.client.get('https://min-api.cryptocompare.com/data/price?fsym=ETH&tsyms=BTC,USD,EUR')
      .pipe(retry(3))
      .subscribe(data => {
        this.priceInEur = (data as any)['EUR']
        console.log(data)
      },
        (err: HttpErrorResponse) => {
          if (err.error instanceof Error) {
            console.log("Client-Error")
          } 
          else {
            console.log("Server-Error")
          }
        })
  }

}
